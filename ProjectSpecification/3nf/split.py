import pandas as pd

def m2seconds(m : str):
    try:
        minute, second = map(int, m.split(":"))
        return minute*60 + second
    except AttributeError:
        #if its null
        return m

#Assign feature id as dict
featuredict = {}
featuredata = pd.read_csv("featuredict.csv", sep=";")
for i, row in featuredata.iterrows():
    featuredict[row['feature']] = row['id']

data = pd.read_csv("AoLR Data collection 2017.csv")

#Time to Int
data['1st pump attendance'] = data['1st pump attendance'].apply(lambda x: m2seconds(x))
data['2nd pump attendance'] = data['2nd pump attendance'].apply(lambda x: m2seconds(x))
data['3rd pump attendance'] = data['3rd pump attendance'].apply(lambda x: m2seconds(x))

#split borough  and ward tables.
borough = data[['Borough code', 'Borough name']].groupby('Borough code').first()
ward = data[['Ward Code', 'Ward name', 'Borough code']]

borough.to_csv("Borough.csv")
ward.to_csv("Ward.csv", index = False)

#create ward2features table
w2f = open("ward2feature.csv","w")

for i, row in data.iterrows():
    ward_code = row["Ward Code"]
    for name, col in row[4:].iteritems():
        try:
            col = int(col)
            newRow = "{wardcode},{fid},{value}\n".format(wardcode = ward_code, fid = featuredict[name], value = col)
        except ValueError:
            #if its null
            newRow = "{wardcode},{fid},NULL\n".format(wardcode = ward_code, fid = featuredict[name])
        
        w2f.write(newRow)
