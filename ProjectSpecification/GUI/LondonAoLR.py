import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import dbConnection as db
from PyQt5.QtGui import *

#Qt Application
app = QApplication(sys.argv)

class ConnectionWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('London Assesment of Local Risk')
        self.setGeometry(250, 250, 480, 180)

        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.usernameText = QLineEdit("Username")
        self.layout.addWidget(self.usernameText)

        self.passwText = QLineEdit("Password")
        self.passwText.setEchoMode(QLineEdit.Password)
        self.layout.addWidget(self.passwText)

        self.sendButton = QPushButton("-Connect-")
        self.sendButton.clicked.connect(self.connectEvent)
        self.layout.addWidget(self.sendButton)

    def connectEvent(self):
        main = MainWindow(self.usernameText.text(), self.passwText.text())
        main.show()
        #self.closeEvent()
        self.close()
        
class MainWindow(QWidget):
    columns = None
    table = "All Tables"
    def __init__(self, username, password):
        super().__init__()
        self.setWindowTitle('London Assessment of Local Risk')
        self.setGeometry(100, 100, 1600, 900)

        #database connection
        self.data = db.LondonData()
        self.data.connect(username, password)
        
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        #Main layout. Tables/TableWidget/Insert-delete/Procedures
        self.layout.addWidget(QLabel("Tables"))

        self.toolbar = QHBoxLayout()
        self.layout.addLayout(self.toolbar)

        self.tableWidget = QTableWidget()
        self.layout.addWidget(self.tableWidget)

        self.layout.addWidget(QLabel("Insert"))

        self.insertTable = QTableWidget()
        self.insertTable.setFixedHeight(80)
        self.layout.addWidget(self.insertTable, 0)

        self.bottomToolbar = QHBoxLayout()
        self.layout.addLayout(self.bottomToolbar)

        self.layout.addWidget(QLabel("Stored Procedures:"))

        self.procToolbar = QHBoxLayout()
        self.layout.addLayout(self.procToolbar)

        #Toolbar Layout (tables)
        self.setToolbar(self.data.showTables())
        
        #Set Table (default -> All Tables)
        self.setTable(self.table)

        #Bottom Toolbar Layout (delete, insert)
        delInfo = QLabel("Delete this row: ")
        delInfo.setFixedWidth(75)
        self.bottomToolbar.addWidget(delInfo)

        self.deleteText = QLineEdit()
        self.deleteText.setFixedWidth(35)
        self.bottomToolbar.addWidget(self.deleteText)

        self.deleteButton = QPushButton("Delete")
        self.deleteButton.clicked.connect(self.deleteEvent)
        self.bottomToolbar.addWidget(self.deleteButton)

        self.insertButton = QPushButton("Insert")
        self.insertButton.clicked.connect(self.insertEvent)
        self.bottomToolbar.addWidget(self.insertButton)

        #Procedure Toolbar
        self.isitsafeText = QLineEdit()
        self.isitsafeButton = QPushButton("Is It Safe?")
        self.isitsafeButton.clicked.connect(self.isItSafeEvent)
        self.safestwardText = QLineEdit()
        self.safestwardButton = QPushButton("Safest Ward of Borough?")
        self.safestwardButton.clicked.connect(self.safestWardOfBoroughEvent)
        self.resultsLabel = QLabel()
        self.resultsLabel.setFixedWidth(250)

        self.procToolbar.addWidget(self.isitsafeText)
        self.procToolbar.addWidget(self.isitsafeButton)
        self.procToolbar.addWidget(self.safestwardText)
        self.procToolbar.addWidget(self.safestwardButton)
        self.procToolbar.addWidget(self.resultsLabel)       

    def setToolbar(self, tables):
        for i in tables:
            tablebutton = QPushButton(i[0])
            tablebutton.clicked.connect(lambda x, table=i[0]: self.setTable(table))
            self.toolbar.addWidget(tablebutton)

    def setTable(self, tableName):
        #Get columns names and dataset from database
        self.table = tableName
        self.columns, dataset = self.data.getTable(tableName)

        #Set table size as data.
        self.tableWidget.setRowCount(len(dataset))
        self.tableWidget.setColumnCount(len(dataset[0]))
        #Fill the table.
        for i in range(len(dataset)):
            for j in range(len(dataset[0])):
                self.tableWidget.setItem(i, j, QTableWidgetItem(str(dataset[i][j])))
            #self.tableWidget.resizeColumnToContents(i)
        self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        #Set column heads.
        print(self.columns)
        self.tableWidget.setHorizontalHeaderLabels(self.columns)

        if self.table == "All Tables":
            self.insertTable.setRowCount(0)
            self.insertTable.setColumnCount(0)
        else:
            self.insertTable.setRowCount(1)
            self.insertTable.setColumnCount(len(self.columns))
            self.insertTable.setHorizontalHeaderLabels(self.columns)        

    def safestWardOfBoroughEvent(self):
        #Call safestWardOfBorough stored procedure.
        if len(self.safestwardText.text()) == 9:
            self.resultsLabel.setText("Geocode of Safest Ward of Borough: {}".format(self.data.safestWardOfBorough(self.safestwardText.text())))

    def isItSafeEvent(self):
        #Call isItSafe stored procedure.
        if len(self.isitsafeText.text()) == 9:
            self.resultsLabel.setText("Is this Ward/Borough safe? : {}".format("No" if self.data.isItSafe(self.isitsafeText.text()) == 0 else "Yes"))

    def insertEvent(self):
        newRow = list()
        #insert row to current table if all cells filled.
        for i in range(len(self.columns)):
            if self.insertTable.item(0, i):
                newRow.append(self.insertTable.item(0, i).text())
            else:
                print("Please fill all cells.")
                return
        self.data.insert(self.table, tuple(newRow))
        self.setTable(self.table)

    def deleteEvent(self):
        #Delete i'th row from table. 
        try: 
            row = int(self.deleteText.text())
            if row > 0 and self.tableWidget.item(row -1, 0):
                #Get pk of row. Pk is always 0 indexed column.
                pk = self.tableWidget.item(row -1, 0).text()
                self.data.delete(self.table, self.columns[0], pk)
                print("Row deleted")
                self.setTable(self.table)
            else:
                return
        except ValueError:
            return

    def closeEvent(self, event):
        self.data.close()
        print("Database connection closed.")
        event.accept()

main = ConnectionWindow()
main.show()


sys.exit(app.exec_())
