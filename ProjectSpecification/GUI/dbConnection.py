import mysql.connector

class LondonData:
    isConnected = False

    def connect(self, usern, pasw):
        #Create connetion and cursor
        self.connection = mysql.connector.connect(host="localhost", user= usern, password=pasw, database="londonaolr")
        self.cursor = self.connection.cursor()
        self.isConnected =  True

    def getTable(self, tableName):
        #Get table according to table name
        if tableName == "All Tables":
            return self.getAllTable()
        
        self.cursor.execute("select * from {}".format(tableName))

        columnName = list()
        for i in self.cursor.description:
            columnName.append(i[0])
            
        return columnName, list(self.cursor)

    def getAllTable(self):
        #Query for all tables join.
        self.cursor.execute('''
        select ward.ward_code, ward_name, ward.borough_code, borough_name, feature_name, feature_value
        from ward
            join borough on ward.borough_code = borough.borough_code
            join ward2feature on ward.ward_code = ward2feature.ward_code
            join features on features.feature_id = ward2feature.feature_id;
        ''')
        columns = "Ward Code, Ward Name, Borough Code, Borugh Name, Feature Type, Value".split(",")

        return columns, list(self.cursor)

    def showTables(self):
        self.cursor.execute('show TABLES')
        return [("All Tables",)] + list(self.cursor)

    def close(self):
        self.connection.close()

    def insert(self, tableName, newRow):
        insert = "insert into {} values (%s, %s)".format(tableName)
        self.cursor.execute(insert, newRow)
        self.connection.commit()

    def delete(self, tableName, pkName, pk):
        delete = "delete from {} where {} = '{}'".format(tableName, pkName, pk)
        self.cursor.execute(delete)
        self.connection.commit()

    #This two are stored procedures.
    def isItSafe(self, geocode : str):
        results = self.cursor.callproc("isItSafe", [geocode, False])
        return results[1]

    def safestWardOfBorough(self, geocode : str):
        results = self.cursor.callproc("safestWardOfBorough", [geocode])
        return results[0]

if __name__ == "__main__":
    database = LondonData()
    database.connect("root", "passw")
    #To test
    #database.insert("borough", ("E11111111","Kahramanmaraş"))
    #database.delete("borough", "borough_code", "E11111111")
    #database.safestWardOfBorough("E09000002")
    database.close()