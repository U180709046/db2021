
-- isITSAFE proc
set @avgconcern = 0;
set @avgconsequences = 0;

select round(avg(feature_value)) into @avgconcern
from ward2feature
where feature_id = (select feature_id from features where feature_name = "concerns_score");

select round(avg(feature_value)) into @avgconsequences
from ward2feature
where feature_id = (select feature_id from features where feature_name = "consequences_score");

select @avgconcern;
select @avgconsequences;

set @geocode = "E09000024";
set @isSafe = True;

set @currentWardConcern = 0;
set @currentWardConsequences = 0;

select feature_value into @currentWardConcern
from ward2feature
where ward_code = @geocode and feature_id = (select feature_id from features where feature_name = "concerns_score");

select feature_value into @currentWardConsequences
from ward2feature
where ward_code = @geocode and feature_id = (select feature_id from features where feature_name = "consequences_score");



select @isSafe;

call isItSafe(@geocode, @isSafe);

select * from borough_risks;
select * from ward2feature where feature_id = 2 order by feature_value;


-- safest Ward of borough procedure

select *
from ward join ward2feature on ward.ward_code = ward2feature.ward_code
where feature_id = (select feature_id from features where feature_name = "concerns_score") and borough_code = "E09000024"
order by feature_value asc
limit 1;

call safestWardOfBorough(@geocode);
select @geocode;

-- bestPlaceForFamily

select ward_code
from ward2feature
where feature_id = (select feature_id from features where feature_name = "students") or feature_id = (select feature_id from features where feature_name = "age_65")
group by ward_code
order by sum(feature_value);


call bestPlaceForFamily(True, True, True, True, @geocode);
select @geocode;

-- GUI query

select ward.ward_code, ward_name, ward.borough_code, borough_name, feature_name, feature_value
from ward
	join borough on ward.borough_code = borough.borough_code
    join ward2feature on ward.ward_code = ward2feature.ward_code
    join features on features.feature_id = ward2feature.feature_id;