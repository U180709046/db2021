PROCEDURE `bestPlaceForFamily`(IN anyAge65 boolean, IN anyStudent boolean, IN anyDisability boolean, IN isInSpecificRegion boolean, INOUT geocode char(9))
BEGIN
	select ward.ward_code into geocode
	from ward join ward2feature on ward.ward_code = ward2feature.ward_code
	where 
		((feature_id = (select feature_id from features where feature_name = "students") and anyStudent) or 
        feature_id = (select feature_id from features where feature_name = "age_65" and anyAge65) or 
         feature_id = (select feature_id from features where feature_name = "disability" and anyDisability)) and
         if(isInSpecificRegion and substr(geocode, 1, 3) = "E09", geocode = ward.borough_code, True)
	group by ward.ward_code
	order by sum(feature_value)
    limit 1;
END