-- 1.	Which borough in London has the largest smoking population?
select borough_code, sum(feature_value)
from ward join ward2feature on ward.ward_code = ward2feature.ward_code
where feature_id = (select feature_id from features where feature_name = "smokers")
group by borough_code
order by sum(feature_value) desc
limit 1;

-- 2.	Which ward in Bexley has the most concern (hazard) score?
select ward_name
from ward join ward2feature on ward.ward_code = ward2feature.ward_code
where feature_id =  (select feature_id from features where feature_name = "concerns_score") and 
feature_value = (
	select max(feature_value)
	from ward2feature
	where feature_id = (select feature_id from features where feature_name = "concerns_score"));

-- 3.	5 wards where the first fire engine to arrive at the scene of an incident in the fastest time?
select * from features;

select * 
from ward2feature
where feature_id = (select feature_id from features where feature_name = "first_response")
order by feature_value
limit 5;

-- 4.	Which is the most populous ward in London?

select ward_name
from ward
where ward_code = (
	select ward_code
    from ward2feature
    where feature_id = (select feature_id from features where feature_name = "population")
    order by feature_value desc
    limit 1);
    
-- 5.	Which wards have a population density below 5000??

select ward_name
from ward join ward2feature on ward.ward_code = ward2feature.ward_code
where feature_id = (select feature_id from features where feature_name = "density") and feature_value < 5000;
 
-- 6.	Which ward has the most traffic accidents?

select ward_name
from ward join ward2feature on ward.ward_code = ward2feature.ward_code
where feature_id = (select feature_id from features where feature_name = "rtc")
order by feature_value desc
limit 1;

-- 7.	What is the student population of wards with a population below a 10000?

select b.ward_code, b.feature_value as "student pop"
from ward2feature as a join ward2feature as b on a.ward_code = b.ward_code
where a.feature_id = (select feature_id from features where feature_name = "population") and a.feature_id < 10000
	and b.feature_id = (select feature_id from features where feature_name = "students");
-- 8.	How many people rescued/released by LFB in wards of Greenwich?

select sum(feature_value)
from ward join ward2feature on ward2feature.ward_code = ward.ward_code
where feature_id = (select feature_id from features where feature_name = "rescue") and
	borough_code = (select borough_code from borough where borough_name = "Greenwich");
    
-- 9.	Wards with more than 10 traffic accidents?

select ward_name
from ward join ward2feature on ward.ward_code = ward2feature.ward_code
where feature_id = (select feature_id from features where feature_name = "rtc") and feature_value > 10;

-- 10.	How many fires broke out in wards with a population of less than 10000 and smoker population above 1000?

select sum(c.feature_value)
from ward2feature as a 
	join ward2feature as b on a.ward_code = b.ward_code
    join ward2feature as c on a.ward_code = c.ward_code
where 
	a.feature_id = (select feature_id from features where feature_name = "population") and a.feature_value < 10000 and
	b.feature_id = (select feature_id from features where feature_name = "smokers") and b.feature_value > 1000 and
    (c.feature_id = (select feature_id from features where feature_name = "pump_fire_1") or 
	c.feature_id = (select feature_id from features where feature_name = "pump_fire_2"));
