-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema LondonAoLR
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `LondonAoLR` ;

-- -----------------------------------------------------
-- Schema LondonAoLR
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `LondonAoLR` DEFAULT CHARACTER SET utf8 ;
USE `LondonAoLR` ;

-- -----------------------------------------------------
-- Table `Borough`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Borough` ;

CREATE TABLE IF NOT EXISTS `Borough` (    -- Sample Data
  `borough_code` CHAR(9) NOT NULL,        -- E09000002 (PK)
  `borough_name` VARCHAR(45) NOT NULL,    -- Barking and Dagenham
  PRIMARY KEY (`borough_code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ward`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Ward` ;

CREATE TABLE IF NOT EXISTS `Ward` (     -- Sample Data
  `ward_code` CHAR(9) NOT NULL,         -- E05000026 (PK)
  `ward_name` VARCHAR(45) NOT NULL,     -- Abbey
  `borough_code` CHAR(9) NOT NULL,      -- E09000002 (FK)
  PRIMARY KEY (`ward_code`),
  CONSTRAINT `fk_Ward_Borough1`
    FOREIGN KEY (`borough_code`)
    REFERENCES `Borough` (`borough_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Ward_Borough1_idx` ON `Ward` (`borough_code` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `Features`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Features` ;

CREATE TABLE IF NOT EXISTS `Features` (  -- Sample Data
  `feature_id` INT NOT NULL,             -- 1
  `feature_name` VARCHAR(45) NOT NULL,   -- concerns_score
  PRIMARY KEY (`feature_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ward2Feature`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Ward2Feature` ;

CREATE TABLE IF NOT EXISTS `Ward2Feature` (-- Sample Data
  `ward_code` CHAR(9) NOT NULL,         -- E05000026 
  `feature_id` INT NOT NULL,            -- 1
  `value` INT NULL,                     -- 31
  PRIMARY KEY (`feature_id`, `ward_code`),
  CONSTRAINT `fk_Ward2Feature_Features1`
    FOREIGN KEY (`feature_id`)
    REFERENCES `Features` (`feature_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ward2Feature_Ward1`
    FOREIGN KEY (`ward_code`)
    REFERENCES `Ward` (`ward_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Ward2Feature_Features1_idx` ON `Ward2Feature` (`feature_id` ASC) VISIBLE;

CREATE INDEX `fk_Ward2Feature_Ward1_idx` ON `Ward2Feature` (`ward_code` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
