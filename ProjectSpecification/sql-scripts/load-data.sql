use londonaolr;

load data
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\laolr\\Borough.csv"
into table borough
columns terminated by ",";

select * from borough;

load data
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\laolr\\Ward.csv"
into table ward
columns terminated by ";";

select * from ward;
select count(ward_code) from ward;

load data
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\laolr\\Features.csv"
into table features
columns terminated by ";";

select * from features;

load data
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\laolr\\ward2feature.csv"
into table ward2feature
columns terminated by ","
(@vward_code, @vfeature_id, @vfeature_value)
set ward_code = @vward_code,
	feature_id = @vfeature_id,
    feature_value = NULLIF(@vfeature_value, "NULL\r");

select count(*) from ward2feature;
select * from ward2feature;
select * from ward2feature where feature_value is NULL;
truncate ward2feature;


/*to fix escape sequence in borough_name data*/
update borough set borough_name = replace(borough_name, '\r', '');
update features set feature_name = replace(feature_name, '\r', '');
