PROCEDURE `isItSafe`(IN geocode char(9), OUT isSafe boolean)
BEGIN
	set @avgconcern = 0;
	set @avgconsequences = 0;
	set @currentConcern = 0;
	set @currentConsequences = 0;
    
	IF substr(geocode, 1, 3) = "E09" then
		select round(avg(feature_value)) into @avgconcern
		from borough_risks
		where feature_id = (select feature_id from features where feature_name = "concerns_score");

		select round(avg(feature_value)) into @avgconsequences
		from borough_risks
		where feature_id = (select feature_id from features where feature_name = "consequences_score");
	
		select feature_value into @currentConcern
		from borough_risks
		where borough_code = geocode and feature_id = (select feature_id from features where feature_name = "concerns_score");

		select feature_value into @currentConsequences
		from borough_risks
		where borough_code = geocode and feature_id = (select feature_id from features where feature_name = "consequences_score");
		        
    ELSE
		select round(avg(feature_value)) into @avgconcern
		from ward2feature
		where feature_id = (select feature_id from features where feature_name = "concerns_score");

		select round(avg(feature_value)) into @avgconsequences
		from ward2feature
		where feature_id = (select feature_id from features where feature_name = "consequences_score");
		
		select feature_value into @currentConcern
		from ward2feature
		where ward_code = geocode and feature_id = (select feature_id from features where feature_name = "concerns_score");

		select feature_value into @currentConsequences
		from ward2feature
		where ward_code = geocode and feature_id = (select feature_id from features where feature_name = "consequences_score");
    END IF;

	set isSafe = if(@currentConcern < @avgconcern and @currentConsequences < @avgconsequences, True, False);
END