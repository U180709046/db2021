PROCEDURE `safestWardOfBorough`(INOUT geocode char(9))
BEGIN
	select ward.ward_code into geocode
	from ward join ward2feature on ward.ward_code = ward2feature.ward_code
	where feature_id = (select feature_id from features where feature_name = "concerns_score") and borough_code = geocode
	order by feature_value asc
	limit 1;

END