drop view borough_risks;


create view borough_risks as
select borough_code, feature_id, round(avg(feature_value)) as feature_value
from ward join ward2feature on ward.ward_code = ward2feature.ward_code
where feature_id = (select feature_id from features where feature_name = "concerns_score") or
	feature_id = (select feature_id from features where feature_name = "consequences_score") or 
    feature_id = (select feature_id from features where feature_name = "control_score")
group by borough_code, feature_id;

select * from borough_risks;